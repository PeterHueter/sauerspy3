package sauerspy3

import (
	"bitbucket.org/PeterHueter/sauerspy3/server"
	"bitbucket.org/PeterHueter/sauerspy3/server/stream"
	"fmt"
	"net"
	"time"
)

// Players send extinfo request for list of players on server
func Players(addr string) (server.Playerstats, error) {
	conn, err := net.Dial("udp", addr)
	if err != nil {
		return nil, fmt.Errorf("error connecting to server %q: %v", addr, err)
	}
	defer conn.Close()
	conn.SetReadDeadline(time.Now().Add(time.Second * 2))

	s := stream.New(conn, time.Second*10)
	srv := server.New(s)
	return srv.PlayerStats(server.ExtAllplayers)
}
