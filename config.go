package sauerspy3

import "bitbucket.org/PeterHueter/sauerspy3/database/driver"

// Config is storage for allconfigs
type Config struct {
	Main struct {
		Master string   `toml:"master"`
		Append []string `toml:"append"`
	} `toml:"main"`

	// Driver settings, can be multiply drivers of same types
	// but with different names
	Databases struct {
		Config map[string]driver.Config `toml:"config"`
	} `toml:"databases"`
}
