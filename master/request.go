package master

import (
	"bytes"
	"fmt"
	"io"
	"time"
)

// Timeout is request timeout error
var Timeout = newError("Timeout")

// DefaultAddr of default master server
const DefaultAddr string = "master.sauerbraten.org:28787"
const defaultTimeout = time.Second * 15

// NewWithTimeout create request with specified timeout
func NewWithTimeout(conn io.ReadWriter, timeout time.Duration) *Request {
	return &Request{
		rw:      conn,
		timeout: timeout,
	}
}

// New create master server request with default timeout
func New(conn io.ReadWriter) *Request {
	return NewWithTimeout(conn, defaultTimeout)
}

// Request is type for master request
type Request struct {
	rw      io.ReadWriter
	timeout time.Duration
}

// Do is send raw request to master server
func (r *Request) Do(req string) ([]byte, error) {
	// request
	_, err := fmt.Fprint(r.rw, req)
	if err != nil {
		return nil, fmt.Errorf("error sending request: %v", err)
	}

	// response
	buf := bytes.Buffer{}

	// Read with timeout can recive send nil or error
	done := make(chan error)
	go func() {
		_, err := io.Copy(&buf, r.rw)
		done <- err
	}()

	select {
	case <-time.After(r.timeout):
		return nil, Timeout
	case err = <-done:
	}

	if err != nil {
		return nil, fmt.Errorf("error read master response: %v", err)
	}
	return buf.Bytes(), nil
}
