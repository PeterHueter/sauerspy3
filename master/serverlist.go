package master

import (
	"bufio"
	"bytes"

	"strconv"
	"strings"
)

// Server is represenation sauer server contain host and port
type Server struct {
	Host string
	Port int
}

// String represent addres in format host:port
func (s *Server) String() string {
	return s.Host + ":" + strconv.Itoa(s.Port)
}

// ExtAddr represend addres in format host:port+1
func (s *Server) ExtAddr() string {
	return s.Host + ":" + strconv.Itoa(s.Port+1)
}

// ServerList fetch servers registered on master in format "ip:port"
// error can be masterserver.Timeout or any other
func (r *Request) ServerList() ([]Server, error) {
	data, err := r.Do("list\n")
	if err != nil {
		return nil, err
	}

	//parse response
	list := make([]Server, 0, 200)
	s := bufio.NewScanner(bytes.NewReader(data))
	for s.Scan() {
		str := s.Text()
		if strings.HasPrefix(str, "addserver") {
			parts := strings.Fields(str)
			if len(parts) != 3 {
				continue
			}
			port, err := strconv.Atoi(parts[2])
			if err != nil {
				continue
			}
			list = append(list, Server{parts[1], port})
		}
	}
	return list, nil
}
