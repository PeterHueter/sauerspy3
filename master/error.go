package master

type errorType struct {
	msg string
}

func (e *errorType) Error() string {
	return e.msg
}

func newError(msg string) *errorType {
	return &errorType{msg}
}
