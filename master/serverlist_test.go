package master

import (
	"bytes"
	"strconv"

	"testing"
)

func TestServerList(t *testing.T) {
	servers := []Server{
		Server{"1.2.3.4", 100},
		Server{"1.2.3.4", 200},
		Server{"1.2.3.4", 300},
		Server{"1.2.3.4", 500},
	}

	// create byte stream
	b := bytes.Buffer{}
	for _, s := range servers {
		b.WriteString("addserver " + s.Host + " " + strconv.Itoa(s.Port) + "\n")
	}
	buf := &testBuf{
		Data: b.Bytes(),
	}

	r := New(buf)

	list, _ := r.ServerList()
	if len(list) != len(servers) {
		t.Errorf("server list not equal: expected %v got %v", len(servers), len(list))
	}

	for i, v := range list {
		if v.String() != servers[i].String() {
			t.Errorf("server not equal: expected %v got %v", servers[i], v)
		}
	}
}
