package master

import (
	"bytes"
	"io"
	"testing"
	"time"
)

// fake ReadWriter
type testBuf struct {
	Data        []byte
	ReadTimeout time.Duration
	done        bool
}

func (b *testBuf) Read(buf []byte) (n int, err error) {
	if b.ReadTimeout != 0 {
		<-time.After(b.ReadTimeout)
	}

	if b.done {
		return 0, io.EOF
	}
	len := copy(buf, b.Data)
	b.done = true
	return len, nil
}

func (b *testBuf) Write(buf []byte) (int, error) {
	return len(buf), nil
}

func TestDo(t *testing.T) {
	// Random testing data
	test := []byte{34, 65, 7, 65, 21, 25, 66}
	buf := &testBuf{
		Data: test,
	}

	r := New(buf)

	res, err := r.Do("list\n")
	if err != nil {
		t.Errorf("error requst list: %v", err)
	}
	ok := bytes.Compare(test, res)
	if ok != 0 {
		t.Errorf("Buffer and data not equal: expected %v got %v", test, res)
	}
}

func TestTimeout(t *testing.T) {

	buf := &testBuf{
		Data:        nil,
		ReadTimeout: time.Second * 2,
	}

	r := NewWithTimeout(buf, time.Second)
	_, err := r.Do("")
	if err == nil {
		t.Errorf("expected timeout but got nothing")
	}
	if err != Timeout {
		t.Errorf("expected timeout but got %v", err)

	}
}
