package sauerspy3

import "bitbucket.org/PeterHueter/sauerspy3/master"
import "net"

// Servers send request to master server for list of registered servers
func Servers() ([]master.Server, error) {
	conn, err := net.Dial("tcp", master.DefaultAddr)
	if err != nil {
		return nil, err
	}
	defer conn.Close()

	return master.New(conn).ServerList()
}
