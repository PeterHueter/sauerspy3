package main

import (
	"log"
	"time"

	spy "bitbucket.org/PeterHueter/sauerspy3"
	"bitbucket.org/PeterHueter/sauerspy3/master"
	"bitbucket.org/PeterHueter/sauerspy3/server"
)

func main() {
	serverChan := make(chan *master.Server, 300)
	playerChan := make(chan *server.PlayerInfo, 300)

	// ask servers for list of players
	go func(s <-chan *master.Server, p chan<- *server.PlayerInfo) {
		for srv := range s {
			// TODO: handle err
			// players on server
			players, err := spy.Players(srv.ExtAddr())
			if err != nil {
				log.Printf("retriving players: %v", err)
				continue
			}

			// pipeline
			for _, player := range players {
				p <- player
			}
		}
	}(serverChan, playerChan)

	// process found players
	go func(c <-chan *server.PlayerInfo) {
		for p := range c {
			log.Printf("%s %s", p.Name, p.IP.String())
		}
	}(playerChan)

	// main loop
	for {
		servers, err := spy.Servers()
		if err != nil {
			log.Fatalf("can not retrive server list: %v", err)
		}

		//	 Range over all servers
		for _, s := range servers {
			go func(c chan *server.PlayerInfo, s master.Server) {
				plrs, err := spy.Players(s.ExtAddr())
				if err != nil {
					log.Println(err)
					return
				}
				for _, p := range plrs {
					c <- p
				}

			}(playerChan, s)
		}

		time.Sleep(time.Second * 10)
	}
}
