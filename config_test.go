package sauerspy3

import (
	"github.com/BurntSushi/toml"
	"strings"
	"testing"
)

func TestConfig(t *testing.T) {
	str := `[main]
			master = "master.sauerbraten.org"
			append = [
				"sauerleague.org:20000",
				"sauerleague.org:30000"
			]
			
			[databases]
			uses = [
				"mysql",
				"log"
			]
			
			[databases.config.mysql]
			driver = "mysql"
			host = "localhost"
			port = 1234
			dbname = "sauerspy"

			[databases.config.remote]
			driver = "mysql"
			host = "localhost"
			port = 1234
			dbname = "sauerspy_remote"
			
			[databases.config.log]
			stdout = true
			`
	var cfg Config
	r := strings.NewReader(str)
	_, err := toml.DecodeReader(r, &cfg)
	if err != nil {
		t.Error(err)
	}

	// mysql values and type casting
	mysql := cfg.Databases.Config["mysql"]
	mustHost := "localhost"
	mustPort := int64(1234)
	mustDbname := "sauerspy"
	host, _ := mysql.String("host")
	port, _ := mysql.Int64("port")
	dbname, _ := mysql.String("dbname")

	if mustHost != host || mustPort != port || mustDbname != dbname {
		t.Error("compare failed:", mustHost, mustPort, mustDbname, mysql, host, port, dbname)
	}

	// log values and type casting
	dblog := cfg.Databases.Config["log"]
	mustStdout := true
	stdout, _ := dblog.Bool("stdout")
	if stdout != mustStdout {
		t.Error("compare failed:", mustStdout, stdout)
	}

	// TODO compare structures
}
