package server

// ExtInfoType is type of request for server
type ExtInfoType int

const (
	// Request types
	infoBasic = 1
	infoExt   = 0

	// Extended request commands
	extUptime      ExtInfoType = 0xFF & 0
	extPlayerstart ExtInfoType = 0xFF & 1
	extTeamscore   ExtInfoType = 0xFF & 2

	// Extended protocol constants
	extAck                  byte = 0xFF & -1
	extNoerror              byte = 0xFF & 0
	extError                byte = 0xFF & 1
	extPlayerstatsRespIds   byte = 0xFF & -10
	extPlayerstatsRespStats byte = 0xFF & -11
	extVersion              byte = 0xFF & 105

	// ExtAllplayers is constatn for all players request in playerstats command
	ExtAllplayers byte = 0XFF & -1
)
