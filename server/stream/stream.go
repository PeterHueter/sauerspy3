package stream

import (
	"bufio"
	"io"
	"time"

	"bitbucket.org/PeterHueter/sauerproto"
)

const maxLen = 512

// Stream is low level abstraction on transport for communicate with sauer server
type Stream struct {
	conn    io.ReadWriter
	timeout time.Duration
}

// New is creating abstractiono above transport conn
func New(conn io.ReadWriter, timeout time.Duration) *Stream {
	return &Stream{
		conn:    conn,
		timeout: timeout,
	}
}

// sendRequest send data and wait for response
func (s *Stream) Write(data []byte) error {
	_, err := s.conn.Write(data)
	if err != nil {
		return err
	}
	return nil
}

// Read is retrive next data packet
func (s *Stream) Read() (*sauerproto.Packet, error) {
	buf := bufio.NewReader(s.conn)

	// TODO read timeout
	raw := make([]byte, maxLen)
	n, err := buf.Read(raw)

	//fmt.Println(raw[:n])

	if err != nil {
		return nil, err
	}

	// trim zeros
	return sauerproto.New(raw[:n]), nil
}
