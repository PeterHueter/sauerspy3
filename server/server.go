package server

import (
	"fmt"
	"io"

	"bitbucket.org/PeterHueter/sauerspy3/server/stream"
)

// Server is represent sauerbraten server protocol abstraction
type Server struct {
	s *stream.Stream
}

// New creates new server which use stream for send reaciving requests
func New(s *stream.Stream) *Server {
	return &Server{s}
}

// BasicInfo return server basic information
func (s *Server) BasicInfo() (*Serverinfo, error) {
	// send basic info request and get answer packet
	err := s.s.Write([]byte{infoBasic})
	if err != nil {
		return nil, err
	}
	p, err := s.s.Read()
	if err != nil {
		return nil, err
	}

	// check header
	t, err := p.Get()
	if err != nil {
		return nil, err
	}
	if t != infoBasic {
		return nil, fmt.Errorf("wrong packet header type, expected %d (BasicInfo) got %d", infoBasic, t)
	}

	return serverinfoFromPacket(p)
}

// Uptime is server uptime in seconds
func (s *Server) Uptime() (Uptime, error) {
	p, err := s.s.Read()
	if err != nil {
		return 0, err
	}
	return uptimeFromPacket(p)
}

// PlayerStats return extended players information, cn is client number,
// if cn = -1 request all players information
func (s *Server) PlayerStats(cn byte) (Playerstats, error) {
	err := sendPlayerinfoRequest(s.s, cn)
	if err != nil {
		return nil, fmt.Errorf("send playerstats request: %v", err)
	}

	// first packet
	p, err := s.s.Read()
	if err != nil {
		return nil, fmt.Errorf("reading packet: %v", err)
	}

	// read header and check if it valid
	err = readPlayerinfoHeader(p, cn)
	if err != nil {
		return nil, fmt.Errorf("header not valid: %v", err)
	}

	// EXT_PLAYERSTATS_RESP_IDS
	b, err := p.Get()
	if err != nil {
		return nil, fmt.Errorf("reading EXT_PLAYERSTATS_RESP_IDS: %v", err)
	}
	if b != extPlayerstatsRespIds {
		return nil, fmt.Errorf("expected %d (EXT_PLAYERSTATS_RESP_IDS) got %d)", extPlayerstatsRespIds, b)
	}

	// count flowing clients
	count := 0
	_, err = p.GetInt()
	for err == nil {
		count++
		_, err = p.GetInt()
	}

	if err != io.EOF {
		return nil, fmt.Errorf("reading client numbers: %v", err)
	}

	// each next packet have header and information about player, we read it all
	players := make(Playerstats, 0, count)
	for i := 0; i < count; i++ {
		p, err = s.s.Read()
		if err != nil {
			return nil, fmt.Errorf("read client packet: %v", err)
		}

		err = readPlayerinfoHeader(p, cn)
		if err != nil {
			return nil, fmt.Errorf("read client packet header: %v", err)
		}

		player, err := readPlayerinfo(p)
		if err != nil {
			return nil, fmt.Errorf("read client information: %v", err)
		}
		players = append(players, player)
	}

	return players, nil
}
