package server

import (
	"fmt"

	"bitbucket.org/PeterHueter/sauerproto"
)

// Serverinfo represent information about game server
type Serverinfo struct {
	Numclients  int
	Proto       int
	Gamemode    int
	Time        int
	Maxclients  int
	Lockmode    int
	Paused      bool
	Speed       int
	Map         string
	Description string
}

func serverinfoFromPacket(p *sauerproto.Packet) (*Serverinfo, error) {
	var err error
	info := &Serverinfo{}

	info.Numclients, err = p.GetInt()
	if err != nil {
		return nil, fmt.Errorf("error processing info packet: %v", err)
	}

	numattrs, err := p.GetInt()
	if err != nil {
		return nil, fmt.Errorf("error processing info packet: %v", err)
	}
	if numattrs != 5 && numattrs != 7 {
		return nil, fmt.Errorf("error expecting 5 or 7 got %d", numattrs)
	}

	attrs := make([]int, 0, 7)
	for i := 0; i < numattrs; i++ {
		val, err := p.GetInt()
		if err != nil {
			return nil, fmt.Errorf("error processing info packet attribute %d: %v", i, err)
		}
		attrs = append(attrs, val)
	}
	info.Proto = attrs[0]
	info.Gamemode = attrs[1]
	info.Time = attrs[2]
	info.Maxclients = attrs[3]
	info.Lockmode = attrs[4]
	if numattrs == 7 {
		info.Paused = attrs[5] == 1
		info.Speed = attrs[6]
	}
	info.Map, err = p.GetString()
	if err != nil {
		return nil, fmt.Errorf("error processing info packet map attribute: %v", err)
	}
	info.Description, err = p.GetString()
	if err != nil {
		return nil, fmt.Errorf("error processing info packet description attribute: %v", err)
	}

	return info, nil
}
