package server

import (
	"fmt"
	"strconv"

	"bitbucket.org/PeterHueter/sauerproto"
	"bitbucket.org/PeterHueter/sauerspy3/server/stream"
)

// IP4 is representation of ip version 4 address
type IP4 [4]byte

// PlayerInfo represent server side player state
type PlayerInfo struct {
	Cn     int
	Ping   int
	Name   string
	Team   string
	Frags  int
	Flags  int
	Deaths int
	Tk     int
	Acc    int
	Hp     int
	Armour int
	Gun    int
	Priv   int
	State  int
	IP     IP4
}

// Playerstats is slice of all players on server
type Playerstats []*PlayerInfo

func (ip *IP4) String() string {
	return strconv.Itoa(int(ip[0])) + "." + strconv.Itoa(int(ip[1])) + "." + strconv.Itoa(int(ip[2])) + "." + strconv.Itoa(int(ip[3]))
}

func sendPlayerinfoRequest(s *stream.Stream, cn byte) error {
	// store request for validate against response
	req := []byte{infoExt, byte(extPlayerstart), cn}
	//fmt.Println("Sent:", req)
	err := s.Write(req)
	if err != nil {
		return fmt.Errorf("sending extinfo request: %v", err)
	}
	return err
}

func readPlayerinfoHeader(p *sauerproto.Packet, cn byte) error {
	// INFO_EXT
	i, err := p.GetInt()
	if err != nil {
		return err
	}
	if ExtInfoType(i) != infoExt {
		return fmt.Errorf("wrong response type, expect %d (INFO_EXT) got %d", infoExt, i)
	}

	// EXT_PLAYERSTATS cmd
	extcmd, err := p.GetInt()
	if err != nil {
		return err
	}
	if extcmd != int(extPlayerstart) {
		return fmt.Errorf("wrong response type, expect %d (EXT_PLAYERSTATS) got %d", extPlayerstart, extcmd)
	}

	// cn
	c, err := p.Get()
	if err != nil {
		return fmt.Errorf("extinfo read response params: %v", err)
	}
	if c != cn {
		return fmt.Errorf("response does not match request: expect %d got %d", cn, c)

	}

	// ACK
	ack, err := p.GetInt()
	if err != nil {
		return err
	}
	if byte(ack) != extAck {
		return fmt.Errorf("expected EXT_ACK (%d) got %d", extAck, ack)
	}

	// EXT_VER
	ver, err := p.Get()
	if err != nil {
		return err
	}
	if ver != extVersion {
		return fmt.Errorf("unsupported extinfo version %d", ver)
	}

	// ERROR
	extErr, err := p.Get()
	if err != nil {
		return err
	}
	if extErr != extNoerror {
		return fmt.Errorf("client requested by id was not found")
	}

	// No errors header is valid
	return nil
}

func readPlayerinfo(p *sauerproto.Packet) (*PlayerInfo, error) {
	player := &PlayerInfo{}

	// EXT_PLAYERSTATS_RESP_STATS
	b, err := p.Get()
	if err != nil {
		return nil, err
	}
	if b != extPlayerstatsRespStats {
		return nil, fmt.Errorf("expected EXT_PLAYERSTATS_RESP_STATS (%d) got %d", extPlayerstatsRespStats, b)
	}

	// cn
	player.Cn, err = p.GetInt()
	if err != nil {
		return nil, fmt.Errorf("reading client number: %v", err)
	}

	// ping
	player.Ping, err = p.GetInt()
	if err != nil {
		return nil, fmt.Errorf("reading ping: %v", err)
	}

	// name
	player.Name, err = p.GetString()
	if err != nil {
		return nil, fmt.Errorf("reading name: %v", err)
	}

	// team
	player.Team, err = p.GetString()
	if err != nil {
		return nil, fmt.Errorf("reading team: %v", err)
	}

	// next 10 parameters is integer values, read them into array
	// then assign to player structure
	params := [10]int{}
	for i := 0; i < 10; i++ {
		params[i], err = p.GetInt()
		if err != nil {
			return nil, fmt.Errorf("reading param 6+%d: %v", i, err)
		}
	}
	player.Frags = params[0]
	player.Flags = params[1]
	player.Deaths = params[2]
	player.Tk = params[3]
	player.Acc = params[4]
	player.Hp = params[5]
	player.Armour = params[6]
	player.Gun = params[7]
	player.Priv = params[8]
	player.State = params[9]

	// read ip in format A.B.C.0
	for i := 0; i < 3; i++ {
		player.IP[i], err = p.Get()
		if err != nil {
			return nil, fmt.Errorf("reading ip octet %d: %v", i, err)
		}
	}

	return player, nil
}
