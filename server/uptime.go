package server

import (
	"time"

	"bitbucket.org/PeterHueter/sauerproto"
)

// Uptime is server running time in seconds
type Uptime int

// Convert in human readable format
func (t Uptime) String() string {
	return time.Duration(time.Second * time.Duration(int(t))).String()
}

func uptimeFromPacket(p *sauerproto.Packet) (Uptime, error) {
	i, err := p.GetInt()
	if err != nil {
		return 0, err
	}

	return Uptime(i), nil
}
