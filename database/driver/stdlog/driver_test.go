package stdlog

import (
	"bitbucket.org/PeterHueter/sauerspy3/database"
	"bitbucket.org/PeterHueter/sauerspy3/database/driver"
	"testing"
	"time"
)

func TestDriver(t *testing.T) {
	cfg := driver.Config{
		"prefix": "[test]",
	}

	err := driver.Initialize(driverName, cfg)
	if err != nil {
		t.Error(err)
	}

	drv, ok := driver.Get(driverName)
	if !ok {
		t.Errorf("driver %s not regisered", driverName)
	}

	p := &database.Player{
		Name: "test player",
		IP:   [4]byte{1, 2, 3, 4},
		Seen: time.Now(),
	}
	drv.Write(p)
}
