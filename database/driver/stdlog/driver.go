package stdlog

import (
	"fmt"
	"io"
	"os"

	"bitbucket.org/PeterHueter/sauerspy3/database"
	"bitbucket.org/PeterHueter/sauerspy3/database/driver"
)

const driverName = "stdlog"

func init() {
	driver.Register(driverName, constructor)
}

func constructor(config driver.Config) (database.Interface, error) {
	var err error
	w := os.Stdout

	path, ok := config.String("file")
	if ok {
		w, err = os.Open(path)
		if err != nil {
			return nil, err
		}
	}

	prefix, ok := config.String("prefix")
	if !ok {
		prefix = "[" + driverName + "]"
	}

	return &stdDriver{
		prefix: prefix,
		w:      w,
	}, nil
}

// Driver is implement database.Interface
type stdDriver struct {
	prefix string
	w      io.Writer
}

func (d *stdDriver) BeginTransaction() error {
	return nil
}

func (d *stdDriver) EndTransaction() error {
	return nil
}

func (d *stdDriver) Rollback() error {
	return nil
}

func (d *stdDriver) Write(p *database.Player) error {
	fmt.Fprintf(d.w, "%s Name: %s\tIp: %d.%d.%d.%d\tSeen: %s\n", d.prefix, p.Name, p.IP[0], p.IP[1], p.IP[2], p.IP[3], p.Seen)
	return nil
}

func (d *stdDriver) ByName(pattern string) []*database.Player {
	return nil
}

func (d *stdDriver) ByIP(ip, mask [4]byte) []*database.Player {
	return nil
}
