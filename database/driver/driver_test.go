package driver

import (
	"testing"
)

func TestConfigInt(t *testing.T) {
	test := map[string]struct {
		param interface{}
		value int64
		ok    bool
	}{
		"0": {
			param: 0,
			value: 0,
			ok:    true,
		},
		"10": {
			param: 10,
			value: 10,
			ok:    true,
		},
		"-100": {
			param: -100,
			value: -100,
			ok:    true,
		},
		"string": {
			param: "string",
			ok:    false,
		},
		"\"42\"": {
			param: "\"42\"",
			ok:    false,
		},
	}

	c := make(Config, len(test))
	for k, v := range test {
		c[k] = v.param
	}

	// testing
	for k, v := range test {
		i, ok := c.Int64(k)
		if ok != v.ok {
			t.Errorf("%q operation result must be %t, but got %t", k, v.ok, ok)
			continue
		}

		if v.ok && v.value != i {
			t.Errorf("value must be %d but got %d", v.value, i)
		}
	}
}
