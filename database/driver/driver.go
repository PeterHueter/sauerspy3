package driver

import (
	"fmt"
	"sync"

	"bitbucket.org/PeterHueter/sauerspy3/database"
)

// Config is represent database configuration
type Config map[string]interface{}

// Constructor is closure which create instance of driver with config
type Constructor func(config Config) (database.Interface, error)

var (
	constructors      = make(map[string]Constructor)
	constructorsMutex sync.RWMutex
	drivers           = make(map[string]database.Interface)
	driversMutex      sync.RWMutex
)

// Int64 return converted to int value from config
// if key not found or can not be converted to int
//then second parameter is false
func (c Config) Int64(key string) (int64, bool) {
	v, ok := c[key]
	if !ok {
		return 0, ok
	}

	i, ok := v.(int64)
	return i, ok
}

// String return converted to string value from config
// if key not found or can not be converted to string
// then second parameter is false
func (c Config) String(key string) (string, bool) {
	v, ok := c[key]
	if !ok {
		return "", ok
	}

	i, ok := v.(string)
	return i, ok
}

// Bool return converted to bool value from config
// if key not found or can not be converted to bool
// then second parameter is false
func (c Config) Bool(key string) (bool, bool) {
	v, ok := c[key]
	if !ok {
		return false, ok
	}

	i, ok := v.(bool)
	return i, ok
}

// Register is add constructor closure to map of constructors,
// must not be called twice for same driver
func Register(name string, c Constructor) {
	constructorsMutex.Lock()
	defer constructorsMutex.Unlock()

	_, ok := constructors[name]
	if ok {
		panic("Register called twice for driver " + name)
	}
	constructors[name] = c
}

// Initialize pass configuration to constructor function
// and create instance of driver, must not be called twice for same driver
func Initialize(name string, c Config) error {
	driversMutex.Lock()
	defer driversMutex.Unlock()

	constructorsMutex.RLock()
	defer constructorsMutex.RUnlock()

	// should not be initialized twice
	_, ok := drivers[name]
	if ok {
		panic("Initialize called twice for driver " + name)
	}

	// constructor should exist
	f, ok := constructors[name]
	if !ok {
		return fmt.Errorf("unknown driver " + name)
	}

	d, err := f(c)
	if err != nil {
		return err
	}
	drivers[name] = d
	return nil
}

// Get is return instance of initialized driver if driver not found return ok = false
func Get(name string) (database.Interface, bool) {
	driversMutex.RLock()
	defer driversMutex.RUnlock()

	d, ok := drivers[name]
	return d, ok
}
