package mysql

import (
	"fmt"

	"database/sql"

	"bitbucket.org/PeterHueter/sauerspy3/database"
	"bitbucket.org/PeterHueter/sauerspy3/database/driver"

	// Mysql support
	_ "github.com/go-sql-driver/mysql"
)

const driverName = "mysql"
const defaultTable = "clients"

func init() {
	driver.Register(driverName, constructor)
}

func constructor(config driver.Config) (database.Interface, error) {
	user, _ := config.String("user")
	pass, _ := config.String("password")
	host, _ := config.String("host")
	dbname, _ := config.String("dbname")
	table, ok := config.String("table")

	// default table name
	if !ok {
		table = defaultTable
	}

	dsn := fmt.Sprintf("%s:%s@%s/%s", user, pass, host, dbname)
	fmt.Println(dsn)

	// connect to db
	db, err := sql.Open("mysql", dsn)
	if err != nil {
		return nil, err
	}

	// prepare insert statement
	stmt, err := db.Prepare("INSERT INTO ?(name, ip, seen) VALUES(?,?,?)")

	return &mysqlDriver{
		db:    db,
		stmt:  stmt,
		table: table,
	}, nil
}

// Driver is implement database.Interface
type mysqlDriver struct {
	db    *sql.DB
	stmt  *sql.Stmt
	tx    *sql.Tx
	table string
}

func (d *mysqlDriver) BeginTransaction() error {
	var err error
	d.tx, err = d.db.Begin()
	return err
}

func (d *mysqlDriver) EndTransaction() error {
	return d.tx.Commit()
}

func (d *mysqlDriver) Rollback() error {
	return d.tx.Rollback()
}

func (d *mysqlDriver) Write(p *database.Player) error {
	_, err := d.stmt.Exec(d.table, p.Name, p.IP32(), p.Seen)
	return err
}

func (d *mysqlDriver) ByName(pattern string) []*database.Player {
	return nil
}

func (d *mysqlDriver) ByIP(ip, mask [4]byte) []*database.Player {
	return nil
}
