package database

import (
	"time"
)

// Player store only necessary information
type Player struct {
	Name string
	IP   [4]byte
	Seen time.Time
}

// Transaction is block writer interface
type Transaction interface {
	BeginTransaction() error
	EndTransaction() error
	Rollback() error
}

// Writer allow to write to database
type Writer interface {
	Write(*Player) error
}

// Finder is allowe search by params
type Finder interface {
	ByName(pattern string) []*Player
	ByIP(ip, mask [4]byte) []*Player
}

// Migration is support database migration interface
type Migration interface {
	CheckSchema() bool
	Migrate() error
}

// Interface is common interface for database
type Interface interface {
	Transaction
	Writer
	Finder
}

// IP32 represent player ip addres as uint value
func (p *Player) IP32() uint32 {
	return uint32(p.IP[0])<<24 | uint32(p.IP[1])<<16 | uint32(p.IP[2])<<8 | uint32(p.IP[3])
}
